---
title: Ryzen 7 1700 + RX570 + Asrock x370
tags:
    - Ryzen 7
    - Desktop with iGPU + dGPU
category: OpenCore
author: Tijmen
time: 4/6/2021
---
## Made by [Tijmen](https://github.com/Tiemon-hoi)

## Cấu hình

* CPU: Ryzen 7 1700
* dGPU: AMD RX570
* Bo mạch chủ: Asrock x370

## What work?

* Hầu hết đều hoạt động

## Tải EFI

<form method="get" action="https://github.com/Cokaps/efi-database/raw/master/Desktop/iGPU%20%2B%20dGPU/OC/Ryzen%207%201700%20%2B%20RX570%20%2B%20Asrock%20x370.zip" class="animate__animated animate__jackInTheBox">
   <button type="submit" class="btn"><i class="fas fa-download"></i> Download EFI</button>
</form>
