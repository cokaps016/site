const { config } = require("vuepress-theme-hope");
const navBarConfig = require("./navBar");

module.exports = config({
  title: "EFI Collection",
  description: "Collection of Clover and OpenCore EFI for Hackintosher✨",
  plugins: {
    // '@vuepress/google-analytics': {
    //   'ga': 'G-GKVQXB8MXE' // UA-00000000-0
    //  }
  },
  head: [
    ["link", {
    rel: "stylesheet",
    href: "https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    }],
    // [
    //   "meta",
    //   {
    //     name: "google-site-verification",
    //     content: "qG3soux9jAKB4Q_DYf7yj1p5cEIuib6yG4zDhpmv2_E",
    //   },
    // ],
    // [
    //   "script",
    //   { src: "https://cdn.jsdelivr.net/npm/@babel/standalone/babel.min.js" },
    // ],
  ],

  dest: "public",
  markdown: {
		extendMarkdown: md => {
			md.use(require("markdown-it-attrs"));
      md.use(require("markdown-it-task-checkbox"));
		}
	},

  locales: {
    "/": { lang: "vi-VN" },
    "/en/": {
      title: "EFI Collection",
      description: "Collection of Clover and OpenCore EFI for Hackintosher✨",
    },
  },

  themeConfig: {
    logo: "/logo.png",
    hostname: "https://vuepress-theme-hope.github.io",

    author: "Cokaps",
    repo: "https://bitbucket.org/cokaps016/database",
    docsRepo: "https://bitbucket.org/cokaps016/site/",
    repoDisplay: true,
    docsBranch: "master",
    docsDir: "blog/",
    editLinkText: "Thêm EFI của bạn!",

    nav: navBarConfig.vi,
    // sidebar: sideBarConfig.vi,
    sidebar: false,

    locales: {
      "/en/": {
        nav: navBarConfig.en,
        sidebar: false,
        // sidebar: sideBarConfig.en,
        editLinkText: "Give us your EFI!",
      },
    },

    // algolia: {
    //   apiKey: "6b0dce9b8ac6778738e26c56def7eb76",
    //   indexName: "vuepress-theme-hope",
    // },

    // algoliaType: "full",

    blog: {
      avatar: "/avatar.png",
      name: "Cokaps",
      intro: "#",
      links: {
        'Bitbucket': 'https://bitbucket.org/cokaps016/',
        'Discord': 'https://discordapp.com/users/845978022732759071',
        'Email': 'cokaps@icloud.com',
        'Facebook': 'https://www.facebook.com/profile.php?id=100068786173924',
        'Github': 'https://github.com/Cokaps',
        'Gitlab': 'https://gitlab.com/cokaps',
        'Instagram': 'https://www.instagram.com/cokaps016/',
        'Linkedin': 'www.linkedin.com/in/cokaps',
        'Pinterest': '',
        'Reddit': 'https://www.reddit.com/u/Cokaps/',
        'Rss': '',
        'Steam': 'https://steamcommunity.com/id/cokaps/',
        'Twitter': 'https://twitter.com/cokaps016/',
      },
    },

    footer: {
      display: true,
      copyright: "MIT Licensed | Copyright © 2021 Cokaps",
    },

    comment: {
      type: "waline",
      serverURL: "https://vuepress-theme-hope-comment.vercel.app",
    },

    copyright: {
      status: "local",
    },

    themeColor: false,

    git: {
      timezone: "Asia/Bangkok",
    },

    mdEnhance: {
      tasklist: true,
      mark: true,
    },

    pwa: {
      favicon: "/favicon.ico",
      themeColor: "#46bd87",
      cachePic: true,
      apple: {
        icon: "/assets/icon/apple-touch-icon.png",
        statusBarColor: "black",
      },
      msTile: {
        image: "/assets/icon/mstile-150x150.png",
        color: "#ffffff",
      },
      manifest: {
        icons: [
          {
            src: "/assets/icon/android-chrome-512x512.png",
            sizes: "512x512",
            purpose: "maskable",
            type: "image/png",
          },
          {
            src: "/assets/icon/android-chrome-192x192.png",
            sizes: "192x192",
            purpose: "maskable",
            type: "image/png",
          },
        ],
        // shortcuts: [
        //   {
        //     name: "Guide",
        //     short_name: "Guide",
        //     url: "/guide/",
        //     icons: [
        //       {
        //         src: "/assets/icon/guide-maskable.png",
        //         sizes: "192x192",
        //         purpose: "maskable",
        //         type: "image/png",
        //       },
        //     ],
        //   },
        // ],
      },
    },
  },
});
